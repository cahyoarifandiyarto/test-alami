package com.test.alami.testalami.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetListTransactionResponse implements Serializable {

    private static final long serialVersionUID = 3717433082741154600L;

    List<GetListTransactionDto> transactions;
}
