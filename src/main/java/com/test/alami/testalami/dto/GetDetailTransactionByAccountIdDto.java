package com.test.alami.testalami.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigInteger;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetDetailTransactionByAccountIdDto implements Serializable {

    private static final long serialVersionUID = 7264952109311376457L;

    private BigInteger amount;

    private String transactionType;

    private String transactionDate;
}
