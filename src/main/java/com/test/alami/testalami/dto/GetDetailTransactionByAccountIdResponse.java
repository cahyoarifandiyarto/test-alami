package com.test.alami.testalami.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetDetailTransactionByAccountIdResponse implements Serializable {

    private List<GetDetailTransactionByAccountIdDto> transactions;
}
