package com.test.alami.testalami.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigInteger;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateTransactionResponse implements Serializable {

    private static final long serialVersionUID = -3661892159460866420L;

    private String transactionId;

    private BigInteger amount;

    private String transactionType;

    private String transactionDate;
}
