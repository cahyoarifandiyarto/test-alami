package com.test.alami.testalami.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigInteger;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetListTransactionDto implements Serializable {

    private static final long serialVersionUID = 4496994930460166502L;

    private String transactionId;

    private String accountName;

    private BigInteger amount;

    private String transactionType;

    private String transactionDate;
}
