package com.test.alami.testalami.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigInteger;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateTransactionRequest implements Serializable {

    private static final long serialVersionUID = -2909028154596171341L;

    @NotBlank
    private String accountId;

    @Min(value = 1, message = "should not be less than 1")
    private BigInteger amount;

    @NotBlank
    private String transactionType;
}
