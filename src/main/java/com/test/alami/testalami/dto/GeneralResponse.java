package com.test.alami.testalami.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GeneralResponse<T> implements Serializable {

    private static final long serialVersionUID = 4142314470241531229L;

    private int code;

    private String status;

    private T data;

    private String error;
}
