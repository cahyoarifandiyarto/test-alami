package com.test.alami.testalami.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetListTransactionRequest implements Serializable {

    private static final long serialVersionUID = 5231694083343939127L;

    private String startDate;

    private String endDate;
}
