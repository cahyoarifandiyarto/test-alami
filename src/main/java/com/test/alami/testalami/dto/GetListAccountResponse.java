package com.test.alami.testalami.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetListAccountResponse implements Serializable {

    private static final long serialVersionUID = 5423424978804507462L;

    private List<AccountDto> accounts = new ArrayList<>();
}
