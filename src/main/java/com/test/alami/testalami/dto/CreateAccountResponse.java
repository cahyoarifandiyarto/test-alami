package com.test.alami.testalami.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateAccountResponse implements Serializable {

    private static final long serialVersionUID = 5103567403568570960L;

    private String id;

    private String fullName;

    private String address;

    private String dateOfBirth;
}
