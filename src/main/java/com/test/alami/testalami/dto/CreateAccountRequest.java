package com.test.alami.testalami.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateAccountRequest implements Serializable {

    private static final long serialVersionUID = -8625611934670188391L;

    @Size(min = 3, message = "should not be less than 3")
    private String fullName;

    @NotBlank
    private String address;

    private String dateOfBirth;
}
