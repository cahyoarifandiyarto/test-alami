package com.test.alami.testalami.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetDetailTransactionByAccountIdRequest implements Serializable {

    private static final long serialVersionUID = 5583331111885612611L;

    private String accountId;
}
