package com.test.alami.testalami.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.Id;

@Document("transaction_history")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionHistory {

    @Id
    private String id;

    @Field("account_id")
    private String accountId;

    @Field("transaction_id")
    private String transactionId;
}
