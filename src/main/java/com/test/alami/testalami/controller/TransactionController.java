package com.test.alami.testalami.controller;

import com.test.alami.testalami.dto.*;
import com.test.alami.testalami.exception.AccountNotFoundException;
import com.test.alami.testalami.exception.InvalidDateException;
import com.test.alami.testalami.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public GeneralResponse<CreateTransactionResponse> createTransaction(@RequestBody CreateTransactionRequest request) throws ParseException, AccountNotFoundException {
        return GeneralResponse.<CreateTransactionResponse>builder()
                .code(201)
                .status("CREATED")
                .data(transactionService.createTransaction(request))
                .build();
    }

    @PostMapping("/list")
    @ResponseStatus(HttpStatus.OK)
    public GeneralResponse<GetListTransactionResponse> getListTransaction(@RequestBody GetListTransactionRequest request) throws ParseException, InvalidDateException {
        return GeneralResponse.<GetListTransactionResponse>builder()
                .code(200)
                .status("OK")
                .data(transactionService.getListTransaction(request))
                .build();
    }
}
