package com.test.alami.testalami.controller;

import com.test.alami.testalami.dto.GeneralResponse;
import com.test.alami.testalami.exception.AccountNotFoundException;
import com.test.alami.testalami.exception.InvalidDateException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;

@RestControllerAdvice
public class ErrorHandlerController {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {ConstraintViolationException.class})
    public GeneralResponse<String> validationHandler(ConstraintViolationException e) {
        return GeneralResponse.<String>builder()
                .code(400)
                .status("BAD_REQUEST")
                .error(e.getMessage())
                .build();
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = {AccountNotFoundException.class})
    public GeneralResponse<String> accountNotFoundHandler(AccountNotFoundException e) {
        return GeneralResponse.<String>builder()
                .code(404)
                .status("NOT_FOUND")
                .error(e.getMessage())
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {InvalidDateException.class})
    public GeneralResponse<String> invalidDateHandler(InvalidDateException e) {
        return GeneralResponse.<String>builder()
                .code(400)
                .status("BAD_REQUEST")
                .error(e.getMessage())
                .build();
    }
}
