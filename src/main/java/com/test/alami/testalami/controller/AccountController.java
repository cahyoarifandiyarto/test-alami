package com.test.alami.testalami.controller;

import com.test.alami.testalami.dto.*;
import com.test.alami.testalami.exception.AccountNotFoundException;
import com.test.alami.testalami.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@RestController
@RequestMapping("/accounts")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public GeneralResponse<CreateAccountResponse> createAccount(@RequestBody CreateAccountRequest request) throws ParseException {
        return GeneralResponse.<CreateAccountResponse>builder()
                .code(201)
                .status("CREATED")
                .data(accountService.createAccount(request))
                .build();
    }

    @GetMapping("/list")
    @ResponseStatus(HttpStatus.OK)
    public GeneralResponse<GetListAccountResponse> getListAccount() {
        return GeneralResponse.<GetListAccountResponse>builder()
                .code(200)
                .status("OK")
                .data(accountService.getListAccount())
                .build();
    }

    @PostMapping("/transaction")
    @ResponseStatus(HttpStatus.OK)
    public GeneralResponse<GetDetailTransactionByAccountIdResponse> getDetailTransaction(@RequestBody GetDetailTransactionByAccountIdRequest request) throws AccountNotFoundException {
        return GeneralResponse.<GetDetailTransactionByAccountIdResponse>builder()
                .code(200)
                .status("OK")
                .data(accountService.getDetailTransactionByAccountId(request))
                .build();
    }
}
