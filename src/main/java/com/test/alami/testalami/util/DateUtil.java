package com.test.alami.testalami.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static Date parseStringToDate(String stringDate) throws ParseException {
        return dateFormat.parse(stringDate);
    }

    public static String parseDateToString(Date date) {
        return dateFormat.format(date);
    }

    public static Date parseDateNowFormat(Date date) throws ParseException {
        return dateFormat.parse(dateFormat.format(date));
    }
}
