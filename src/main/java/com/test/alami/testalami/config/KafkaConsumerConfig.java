package com.test.alami.testalami.config;

import com.test.alami.testalami.dto.GetDetailTransactionByAccountIdRequest;
import com.test.alami.testalami.entity.TransactionHistory;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
public class KafkaConsumerConfig {

    @Value("${kafka.bootstrapAddress}")
    private String bootstrapAddress;

    @Value("${kafka.consumer.groupId}")
    private String groupId;

    @Bean
    public ConsumerFactory<String, TransactionHistory> transactionHistoryConsumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);

        return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(), new JsonDeserializer<>(TransactionHistory.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, TransactionHistory> transactionHistoryConcurrentKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, TransactionHistory> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(transactionHistoryConsumerFactory());

        return factory;
    }

    @Bean
    public ConsumerFactory<String, GetDetailTransactionByAccountIdRequest> getDetailTransactionByAccountIdConsumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);

        return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(), new JsonDeserializer<>(GetDetailTransactionByAccountIdRequest.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, GetDetailTransactionByAccountIdRequest> getDetailTransactionByAccountIdConcurrentKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, GetDetailTransactionByAccountIdRequest> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(getDetailTransactionByAccountIdConsumerFactory());

        return factory;
    }
}
