package com.test.alami.testalami.repository;

import com.test.alami.testalami.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<Transaction, String> {

}
