package com.test.alami.testalami.repository;

import com.test.alami.testalami.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, String> {
}
