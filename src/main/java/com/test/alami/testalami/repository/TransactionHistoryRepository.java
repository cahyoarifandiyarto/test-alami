package com.test.alami.testalami.repository;

import com.test.alami.testalami.entity.TransactionHistory;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TransactionHistoryRepository extends MongoRepository<TransactionHistory, String> {

    List<TransactionHistory> findAllByAccountId(String accountId);
}
