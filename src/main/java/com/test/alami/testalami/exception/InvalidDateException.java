package com.test.alami.testalami.exception;

public class InvalidDateException extends Exception {

    public InvalidDateException(String message) {
        super(message);
    }
}
