package com.test.alami.testalami;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestAlamiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestAlamiApplication.class, args);
	}

}
