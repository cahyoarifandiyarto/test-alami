package com.test.alami.testalami.service;

import com.test.alami.testalami.dto.*;
import com.test.alami.testalami.exception.AccountNotFoundException;

import java.text.ParseException;

public interface AccountService {

    CreateAccountResponse createAccount(CreateAccountRequest request) throws ParseException;

    GetListAccountResponse getListAccount();

    GetDetailTransactionByAccountIdResponse getDetailTransactionByAccountId(GetDetailTransactionByAccountIdRequest request) throws AccountNotFoundException;
}
