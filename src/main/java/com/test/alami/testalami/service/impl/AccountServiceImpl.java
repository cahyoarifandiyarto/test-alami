package com.test.alami.testalami.service.impl;

import com.test.alami.testalami.dto.*;
import com.test.alami.testalami.entity.Account;
import com.test.alami.testalami.entity.Transaction;
import com.test.alami.testalami.entity.TransactionHistory;
import com.test.alami.testalami.exception.AccountNotFoundException;
import com.test.alami.testalami.repository.AccountRepository;
import com.test.alami.testalami.repository.TransactionHistoryRepository;
import com.test.alami.testalami.repository.TransactionRepository;
import com.test.alami.testalami.service.AccountService;
import com.test.alami.testalami.util.DateUtil;
import com.test.alami.testalami.validation.Validation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Service
@Log4j2
public class AccountServiceImpl implements AccountService {

    @Value("${kafka.topicGetTransactionHistory}")
    private String topicGetTransactionHistory;

    @Autowired
    Validation validation;

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TransactionHistoryRepository transactionHistoryRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public CreateAccountResponse createAccount(CreateAccountRequest request) throws ParseException {
        validation.validate(request);

        Account accountBuilder = Account.builder()
                .fullName(request.getFullName())
                .address(request.getAddress())
                .dateOfBirth(DateUtil.parseStringToDate(request.getDateOfBirth()))
                .build();

        Account account = accountRepository.save(accountBuilder);

        return CreateAccountResponse.builder()
                .id(account.getId())
                .fullName(account.getFullName())
                .address(account.getAddress())
                .dateOfBirth(DateUtil.parseDateToString(account.getDateOfBirth()))
                .build();
    }

    @Override
    public GetListAccountResponse getListAccount() {
        List<Account> accountList = accountRepository.findAll();

        List<AccountDto> accountDtoList = new ArrayList<>();
        for (Account account : accountList) {
            AccountDto accountDto = AccountDto.builder()
                    .id(account.getId())
                    .fullName(account.getFullName())
                    .address(account.getAddress())
                    .dateOfBirth(DateUtil.parseDateToString(account.getDateOfBirth()))
                    .build();

            accountDtoList.add(accountDto);
        }

        return GetListAccountResponse.builder()
                .accounts(accountDtoList)
                .build();
    }

    @Override
    public GetDetailTransactionByAccountIdResponse getDetailTransactionByAccountId(GetDetailTransactionByAccountIdRequest request) throws AccountNotFoundException {
        List<TransactionHistory> transactionHistories = transactionHistoryRepository.findAllByAccountId(request.getAccountId());
        log.info("Transaction History {}", transactionHistories);

        List<GetDetailTransactionByAccountIdDto> getDetailTransactionByAccountIdDtoList = new ArrayList<>();

        for (TransactionHistory transactionHistory : transactionHistories) {
            String transactionId = transactionHistory.getTransactionId();

            Transaction transaction = transactionRepository.findById(transactionId).get();

            GetDetailTransactionByAccountIdDto getDetailTransactionByAccountIdDto = GetDetailTransactionByAccountIdDto.builder()
                    .amount(transaction.getAmount())
                    .transactionType(transaction.getTransactionType())
                    .transactionDate(DateUtil.parseDateToString(transaction.getTransactionDate()))
                    .build();

            getDetailTransactionByAccountIdDtoList.add(getDetailTransactionByAccountIdDto);
        }

        return GetDetailTransactionByAccountIdResponse.builder()
                .transactions(getDetailTransactionByAccountIdDtoList)
                .build();
    }
}
