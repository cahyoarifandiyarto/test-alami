package com.test.alami.testalami.service;

import com.test.alami.testalami.dto.CreateTransactionRequest;
import com.test.alami.testalami.dto.CreateTransactionResponse;
import com.test.alami.testalami.dto.GetListTransactionRequest;
import com.test.alami.testalami.dto.GetListTransactionResponse;
import com.test.alami.testalami.exception.AccountNotFoundException;
import com.test.alami.testalami.exception.InvalidDateException;

import java.text.ParseException;

public interface TransactionService {

    CreateTransactionResponse createTransaction(CreateTransactionRequest request) throws AccountNotFoundException, ParseException;

    GetListTransactionResponse getListTransaction(GetListTransactionRequest request) throws ParseException, InvalidDateException;
}
