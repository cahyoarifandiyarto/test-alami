package com.test.alami.testalami.service.impl;

import com.test.alami.testalami.dto.*;
import com.test.alami.testalami.entity.Account;
import com.test.alami.testalami.entity.Transaction;
import com.test.alami.testalami.entity.TransactionHistory;
import com.test.alami.testalami.exception.AccountNotFoundException;
import com.test.alami.testalami.exception.InvalidDateException;
import com.test.alami.testalami.repository.AccountRepository;
import com.test.alami.testalami.repository.TransactionHistoryRepository;
import com.test.alami.testalami.repository.TransactionRepository;
import com.test.alami.testalami.service.TransactionService;
import com.test.alami.testalami.util.DateUtil;
import com.test.alami.testalami.validation.Validation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Log4j2
public class TransactionServiceImpl implements TransactionService {

    @Value("${kafka.topicTransactionHistory}")
    private String topicTransactionHistory;

    @Autowired
    private Validation validation;

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionHistoryRepository transactionHistoryRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public CreateTransactionResponse createTransaction(CreateTransactionRequest request) throws AccountNotFoundException, ParseException {
        validation.validate(request);

        Optional<Account> accountOptional = accountRepository.findById(request.getAccountId());

        if (accountOptional.isEmpty()) {
            throw new AccountNotFoundException("Account not found");
        }

        Account account = accountOptional.get();

        Transaction transactionBuilder = Transaction.builder()
                .account(account)
                .amount(request.getAmount())
                .transactionType(request.getTransactionType())
                .transactionDate(DateUtil.parseDateNowFormat(new Date()))
                .build();

        Transaction transaction = transactionRepository.save(transactionBuilder);

        TransactionHistory transactionHistory = TransactionHistory.builder()
                .accountId(account.getId())
                .transactionId(transaction.getId())
                .build();

        sendMessage(transactionHistory);

        return CreateTransactionResponse.builder()
                .transactionId(transaction.getId())
                .amount(transaction.getAmount())
                .transactionType(transaction.getTransactionType())
                .transactionDate(DateUtil.parseDateToString(transaction.getTransactionDate()))
                .build();
    }

    @Override
    public GetListTransactionResponse getListTransaction(GetListTransactionRequest request) throws ParseException, InvalidDateException {
        Date startDate = DateUtil.parseStringToDate(request.getStartDate());
        Date endDate = DateUtil.parseStringToDate(request.getEndDate());
        Date dateNow = DateUtil.parseDateNowFormat(new Date());

        if (startDate.after(dateNow)) {
            throw new InvalidDateException("Invalid Date");
        }

        List<Transaction> transactionList = transactionRepository.findAll();

        List<GetListTransactionDto> transactionDtos = new ArrayList<>();

        for (Transaction transaction : transactionList) {
            log.info("Account {}", transaction.getAccount());

            String dateString = DateUtil.parseDateToString(transaction.getTransactionDate());
            Date transactionDate = DateUtil.parseStringToDate(dateString);

            if (transactionDate.getTime() >= startDate.getTime() && transactionDate.getTime() <= endDate.getTime()) {
                GetListTransactionDto getListTransactionDto = GetListTransactionDto.builder()
                        .transactionId(transaction.getId())
                        .accountName(transaction.getAccount().getFullName())
                        .amount(transaction.getAmount())
                        .transactionType(transaction.getTransactionType())
                        .transactionDate(DateUtil.parseDateToString(transaction.getTransactionDate()))
                        .build();

                transactionDtos.add(getListTransactionDto);
            }
        }

        return GetListTransactionResponse.builder()
                .transactions(transactionDtos)
                .build();
    }

    private void sendMessage(Object message) {
        ListenableFuture<SendResult<String, Object>> future =
                kafkaTemplate.send(topicTransactionHistory, message);

        future.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                System.out.println("Sent message=[" + message +
                        "] with offset=[" + result.getRecordMetadata().offset() + "]");
            }
            @Override
            public void onFailure(Throwable ex) {
                System.out.println("Unable to send message=["
                        + message + "] due to : " + ex.getMessage());
            }
        });
    }

    @KafkaListener(
            topics = "TOPIC-TRANSACTION-HISTORY",
            containerFactory = "transactionHistoryConcurrentKafkaListenerContainerFactory"
    )
    private void transactionHistoryListener(TransactionHistory transactionHistory) {
        transactionHistoryRepository.save(transactionHistory);
    }
}
