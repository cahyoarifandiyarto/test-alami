CREATE TABLE IF NOT EXISTS transactions (
    id VARCHAR(100) PRIMARY KEY,
    account_id VARCHAR(50) NOT NULL,
    amount BIGINT NOT NULL,
    transaction_type VARCHAR(100) NOT NULL,
    transaction_date TIMESTAMP NOT NULL,
    FOREIGN KEY (account_id)
        REFERENCES accounts (id)
);