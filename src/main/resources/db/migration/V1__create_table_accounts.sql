CREATE TABLE IF NOT EXISTS accounts (
    id VARCHAR(100) PRIMARY KEY,
    full_name VARCHAR(50) NOT NULL,
    address VARCHAR(255) NOT NULL,
    date_of_birth DATE NOT NULL
);